#!/bin/bash
outputFile="output.csv"

if [ $# -ne 2 ]; then
	echo "2 parameters of files"
	exit 1
fi

g++ -Wall -pedantic -std=c++11 -o output.exe main.c && {
	data=$(cat $1; echo ""; cat $2)
	
	./output.exe $outputFile <<< $data

	# because of Excel
	sed -i 's|0\.|0,|g' $outputFile
}