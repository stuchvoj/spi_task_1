#include <iostream>
#include "Stat.cpp"
using namespace std;


int main(int argc, char * argv[]) {
	string outputFileName = argc > 1 ? argv[1] : "output.csv";

	string title1, title2;
	string words1, words2;
	getline(cin, title1);
	getline(cin, words1);
	getline(cin, title2);
	getline(cin, words2);

	Stat * s = new Stat();
	s->setData(words1);

	Stat * p = new Stat();
	p->setData(words2);

	FILE * file = fopen(outputFileName.c_str(), "w");
	
	/* TASK 1 ----------------------------------------------------------------- */

	cout << "EX = " << s->getMoment(1) << endl;
	cout << "varX = " << s->getRozptyl() << endl;
	cout << "EY = " << p->getMoment(1) << endl;
	cout << "varY = " << p->getRozptyl() << endl;

//	cout << endl;
	fprintf(file, "\n");
	fprintf(file, "Delka slova;Pravdepodobnost;\n");
	for(int i = 1; i <= s->maxWordLength(); i++) {
//		printf("%d; %f;\n", i, s->pstOf(i));
		fprintf(file, "%d; %f;\n", i, s->pstOf(i));
	}

//	cout << endl;
	fprintf(file, "\n");
	fprintf(file, "Delka slova;Pravdepodobnost;\n");
	for(int i = 1; i <= p->maxWordLength(); i++) {
//		printf("%d; %f;\n", i, p->pstOf(i));
		fprintf(file, "%d; %f;\n", i, p->pstOf(i));
	}


	/* TASK 2 ----------------------------------------------------------------- */

//	cout << endl;
	fprintf(file, "\n");
	fprintf(file, "Pismeno;Pravdepodobnost;\n");
	for (char c = 'a'; c <= 'z'; c++) {
//		printf("%c; %f;\n", c, s->pstOf(c));
		fprintf(file, "%c; %f;\n", c, s->pstOf(c));
	}

//	cout << endl;
	fprintf(file, "\n");
	fprintf(file, "Pismeno;Pravdepodobnost;\n");
	for (char c = 'a'; c <= 'z'; c++) {
//		printf("%c; %f;\n", c, p->pstOf(c));
		fprintf(file, "%c; %f;\n", c, p->pstOf(c));
	}
	

	/* TASK 2 ----------------------------------------------------------------- */
	//printf("x=%s\n", s->vectorWords());
	//printf("y=%s\n", p->vectorWords());

	fprintf(file, "%s\n", s->cetnostiSlovZahlavi());
	fprintf(file, "%s\n", s->cetnostiSlov());
	fprintf(file, "%s\n\n", p->cetnostiSlov());

	fprintf(file, "%s\n", s->cetnostiPismenZahlavi());
	fprintf(file, "%s\n", s->cetnostiPismen());
	fprintf(file, "%s\n\n", p->cetnostiPismen());

	fclose(file);
	return 0;
}