#include <math.h>
#include <map>
#include <vector>
#include <sstream>
using namespace std;

class Stat {
protected:
	vector<string> words;
	map<int, int> metadataWords;
	map<char, int> metadataLetters;
	int wordsCnt = 0;
	int lettersCnt = 0;
public:
	void setData(string data) {
		stringstream ss(data);
		string word;
		while (getline(ss, word, ' ')) {
		    this->words.push_back(word);
		    metadataWords[word.length()]++;
		    this->wordsCnt++;

		    for (unsigned i = 0; i < word.length(); i++) {
		    	metadataLetters[word[i]]++;
		    }
		    this->lettersCnt += word.length();
		}
	}

	int maxWordLength() {
		int max = -1;
		for (auto x : this->metadataWords) {
			max = max > x.first ? max : x.first;
		}
		return max;
	}

	/**
	 * get n'th moment
	 */
	double getMoment(int num) {
		double ex = 0;
		for(auto x : this->metadataWords) {
			double pst = x.second / (double)wordsCnt;
			ex += pow(x.first, num) * pst;
		}
		return ex;
	}

	double getRozptyl() {
		return this->getMoment(2) - this->getMoment(1) * this->getMoment(1);
	}

	double pstOf(char c) {
		return this->metadataLetters[c] / (double)lettersCnt;
	}

	double pstOf(int length) {
		return this->metadataWords[length] / (double)this->wordsCnt;
	}


	/*---------------------------------------------------------------------------------------------*/
	/*--------------------------------------pro třetí úkol-----------------------------------------*/
	/*---------------------------------------------------------------------------------------------*/
	double T_DvouvyberovyTest(Stat * s) {
		int m = this->wordsCount();
		int n = s->wordsCount();
		return ((this->vyberovyPrumer() - s->vyberovyPrumer()) / (this->s12(s))) * pow((n*m) / (n+m), 0.5);
	}

	double vyberovyPrumer() {
		double prumer = 0.0;
		for (auto x : this->words) {
			prumer += x.length();
		}
		return prumer / this->wordsCnt;
	}

	double vyberovyRozptyl() {
		double rozptyl = 0.0;
		double prumer = this->vyberovyPrumer();
		for (auto x : this->words) {
			rozptyl += pow(x.length() - prumer, 2);
		}
		return rozptyl / (this->wordsCnt - 1);
	}

	int wordsCount() { return this->wordsCnt; }

	double s12(Stat * s) {
		double x = ((this->wordsCount() - 1) * this->vyberovyRozptyl()) + ((s->wordsCount() - 1) * s->vyberovyRozptyl());
		double y = this->wordsCount() + s->wordsCount() - 2;
		return pow((x) / (y), 0.5);
	}

	int stupenVolnosti(Stat* s) {
		return this->wordsCount() + s->wordsCount() - 2;
	}

	double kritickyPrah(Stat * s, double alpha) {
		if (this->stupenVolnosti(s) > 60) {
			if (alpha/2 > 0.333)
				return 0.253;
			if (alpha/2 > 0.25)
				return 0.431;
			if (alpha/2 > 0.2)
				return 0.674;
			if (alpha/2 > 0.125)
				return 0.842;
			if (alpha/2 > 0.1)
				return 1.15;
			if (alpha/2 > 0.05)
				return 1.282;
			if (alpha/2 > 0.025)
				return 1.645;
			if (alpha/2 > 0.01)
				return 1.96;
			if (alpha/2 > 0.005)
				return 2.326;
			if (alpha/2 > 0.001)
				return 2.576;
			return 3.09;
		}
		throw new Stat;
	}

	const char * vectorWords() {
		string output = "c(";
		for (int i = 0; i < this->wordsCnt; i++) {
			output.append(to_string(this->words[i].length()));
			if (i+1 != this->wordsCnt) output.append(",");
		}
		return output.append(");").c_str();
	}

	const char * cetnostiSlov() {
		string ret = "";
		for (auto x : this->metadataWords) {
			ret.append(to_string(x.second)).append(";");
		}
		return ret.c_str();
	}

	const char * cetnostiPismen() {
		string ret = "";
		for (auto x : this->metadataLetters) {
			ret.append(to_string(x.second)).append(";");
		}
		return ret.c_str();
	}

	const char * cetnostiSlovZahlavi() {
		string ret = "";
		for (auto x : this->metadataWords) {
			ret.append(to_string(x.first)).append(";");
		}
		return ret.c_str();
	}

	const char * cetnostiPismenZahlavi() {
		char * ret = (char*)malloc(2 * this->metadataLetters.size() * sizeof(char));
		int len = 0;
		for (auto x : this->metadataLetters) {
			ret[2 * len] = x.first;
			ret[2 * len + 1] = ';';
			len++;
		}
		ret[2 * len] = '\0';
		return ret;
	}

};